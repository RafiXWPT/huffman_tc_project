# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mywindow.ui'
#
# Created: Sat Jun 14 00:10:45 2014
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui
from PyQt4.QtGui import * 

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_MyWindow(object):
    def setupUi(self, MyWindow):
        MyWindow.setObjectName(_fromUtf8("MyWindow"))
        MyWindow.setFixedSize(400, 300)
        self.centralwidget = QtGui.QWidget(MyWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
	palette	= QPalette()
    	palette.setBrush(QPalette.Background,QBrush(QPixmap("image.jpg")))
    	MyWindow.setPalette(palette)
	icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8("nic.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    	MyWindow.setWindowIcon(icon)
	self.label = QtGui.QLabel(self.centralwidget)
    	self.label.setGeometry(QtCore.QRect(90, 30, 231, 41))
    	self.label.setObjectName(_fromUtf8("label"))
    	self.pushButton = QtGui.QPushButton(MyWindow)
	self.pushButton.setGeometry(QtCore.QRect(45, 230, 97, 31))
	self.pushButton.setStyleSheet("background-color: cyan")
    	self.pushButton.setObjectName(_fromUtf8("Program1"))
    	self.pushButton_2 = QtGui.QPushButton(MyWindow)
	self.pushButton_2.setGeometry(QtCore.QRect(151, 230, 97, 31))
    	self.pushButton_2.setObjectName(_fromUtf8("pushButton_2"))
	self.pushButton_2.setStyleSheet("background-color: cyan")
    	self.pushButton_3 = QtGui.QPushButton(MyWindow)
	self.pushButton_3.setGeometry(QtCore.QRect(257, 230, 97, 31))
    	self.pushButton_3.setObjectName(_fromUtf8("pushButton_3"))
	self.pushButton_3.setStyleSheet("background-color: cyan")
	icon = QIcon("exit.png")
	self.pushButton_3.setIcon(icon)
	icon = QIcon("kal.png")
	self.pushButton_2.setIcon(icon)
	icon = QIcon("kal.png")
	self.pushButton.setIcon(icon)
        MyWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(MyWindow)
	MyWindow.setStyleSheet("""
           QMenuBar {
               background-color: rgb(49,49,49);
               color: rgb(255,255,255);
               border: 1px solid #000;
           }
 
           QMenuBar::item {
               background-color: rgb(49,49,49);
               color: rgb(255,255,255);
           }
 
           QMenuBar::item::selected {
               background-color: rgb(30,30,30);
           }
 
           QMenu {
               background-color: rgb(49,49,49);
               color: rgb(255,255,255);
               border: 1px solid #000;          
           }
 
           QMenu::item::selected {
               background-color: rgb(30,30,30);
           }
       """)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 439, 20))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        self.menuJnkm = QtGui.QMenu(self.menubar)
        self.menuJnkm.setObjectName(_fromUtf8("menuJnkm"))
        self.menuPomoc = QtGui.QMenu(self.menubar)
        self.menuPomoc.setObjectName(_fromUtf8("menuPomoc"))
        MyWindow.setMenuBar(self.menubar)
        self.statusbar = QtGui.QStatusBar(MyWindow)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        MyWindow.setStatusBar(self.statusbar)
        self.actionKnmk = QtGui.QAction(QtGui.QIcon('kal.png'), 'Zamknij', MyWindow)
        self.actionKnmk.setObjectName(_fromUtf8("actionKnmk"))
	self.actionKnmk.setShortcut('Ctrl+1') 
	self.actionKnmk.setStatusTip('Opcja z domyslnymi parametrami')
        self.actionMl = QtGui.QAction(QtGui.QIcon('kal.png'), 'Zamknij', MyWindow)
        self.actionMl.setObjectName(_fromUtf8("actionMl"))
	self.actionMl.setShortcut('Ctrl+2') 
	self.actionMl.setStatusTip('Opcja z podaniem wlasnych parametrow')
        self.actionZamknij = QtGui.QAction(QtGui.QIcon('exit.png'), 'Zamknij', MyWindow)
        self.actionZamknij.setObjectName(_fromUtf8("actionZamknij"))
	self.actionZamknij.setShortcut('Ctrl+Q') 
	self.actionZamknij.setStatusTip('Wyjscie z aplikacji')
        self.actionInfo = QtGui.QAction(QtGui.QIcon('info.png'), 'Info', MyWindow)
        self.actionInfo.setObjectName(_fromUtf8("actionInfo"))
	self.actionInfo.setShortcut('F1')
	self.actionInfo.setStatusTip('Informacje o programie')
        self.menuJnkm.addAction(self.actionKnmk)
        self.menuJnkm.addAction(self.actionMl)
        self.menuJnkm.addAction(self.actionZamknij)
        self.menuPomoc.addAction(self.actionInfo)
        self.menubar.addAction(self.menuJnkm.menuAction())
        self.menubar.addAction(self.menuPomoc.menuAction())

        self.retranslateUi(MyWindow)
        QtCore.QMetaObject.connectSlotsByName(MyWindow)
	QtCore.QObject.connect(self.pushButton_3, QtCore.SIGNAL(_fromUtf8("clicked()")), MyWindow.close)
	QtCore.QObject.connect(self.actionZamknij, QtCore.SIGNAL('triggered()'), MyWindow.close)

    def retranslateUi(self, MyWindow):
        MyWindow.setWindowTitle(_translate("MyWindow", "Kodowanie Huffmana", None))
        self.menuJnkm.setTitle(_translate("MyWindow", "Program", None))
        self.menuPomoc.setTitle(_translate("MyWindow", "Pomoc", None))
        self.actionKnmk.setText(_translate("MyWindow", "Huffman 1", None))
        self.actionMl.setText(_translate("MyWindow", "Huffman 2 ", None))
	font = QtGui.QFont()
        font.setPointSize(16)
	self.label.setFont(font)
	self.label.setText(_translate("MyWindow", "Witaj w programie !!!", None))
	self.pushButton.setText(_translate("MyWindow", "HUFFMAN 1", None))
    	self.pushButton_2.setText(_translate("MyWindow", "HUFFMAN 2", None))
    	self.pushButton_3.setText(_translate("MyWindow", "Zamknij", None))
