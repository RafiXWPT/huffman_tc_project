# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'drzewo.ui'
#
# Created: Sat Jun 14 13:46:32 2014
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui
from PyQt4.QtGui import * 
from PIL import Image

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Drzewo(object):
    def setupUi(self, Drzewo):
        Drzewo.setObjectName(_fromUtf8("Drzewo"))
	im=Image.open("image.jpg")
	width, height = im.size
	Drzewo.setFixedSize(width, height)
	palette	= QPalette()
    	palette.setBrush(QPalette.Background,QBrush(QPixmap("image.jpg")))
    	Drzewo.setPalette(palette)

        self.retranslateUi(Drzewo)
        QtCore.QMetaObject.connectSlotsByName(Drzewo)

    def retranslateUi(self, Drzewo):
        Drzewo.setWindowTitle(_translate("Drzewo", "Drzewo", None))
