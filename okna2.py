# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'okna2.ui'
#
# Created: Sun May 11 00:59:27 2014
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui
from PyQt4.QtGui import *

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName(_fromUtf8("Dialog"))
        Dialog.setFixedSize(400, 302)
	palette	= QPalette()
    	palette.setBrush(QPalette.Background,QBrush(QPixmap("imag.jpg")))
    	Dialog.setPalette(palette)
        self.pushButton = QtGui.QPushButton(Dialog)
        self.pushButton.setGeometry(QtCore.QRect(300, 80, 75, 31))
        self.pushButton.setObjectName(_fromUtf8("pushButton"))
        self.textBrowser = QtGui.QTextBrowser(Dialog)
        self.textBrowser.setGeometry(QtCore.QRect(20, 70, 251, 192))
        self.textBrowser.setObjectName(_fromUtf8("textBrowser"))
        self.lineEdit = QtGui.QLineEdit(Dialog)
        self.lineEdit.setGeometry(QtCore.QRect(20, 30, 251, 20))
        self.lineEdit.setObjectName(_fromUtf8("lineEdit"))
        self.label = QtGui.QLabel(Dialog)
        self.label.setGeometry(QtCore.QRect(20, 10, 301, 16))
        self.label.setObjectName(_fromUtf8("label"))
        self.pushButton_2 = QtGui.QPushButton(Dialog)
        self.pushButton_2.setGeometry(QtCore.QRect(300, 120, 75, 31))
        self.pushButton_2.setObjectName(_fromUtf8("pushButton_2"))
        self.pushButton_3 = QtGui.QPushButton(Dialog)
        self.pushButton_3.setGeometry(QtCore.QRect(300, 222, 75, 31))
        self.pushButton_3.setObjectName(_fromUtf8("pushButton_3"))
	self.pushButton_4 = QtGui.QPushButton(Dialog)
        self.pushButton_4.setGeometry(QtCore.QRect(300, 25, 75, 31))
        self.pushButton_4.setObjectName(_fromUtf8("pushButton_4"))
	self.pushButton.setStyleSheet("background-color: cyan")
	self.pushButton_2.setStyleSheet("background-color: cyan")
	self.pushButton_3.setStyleSheet("background-color: cyan")
	self.pushButton_4.setStyleSheet("background-color: cyan")
	self.checkBox = QtGui.QCheckBox(Dialog)
        self.checkBox.setGeometry(QtCore.QRect(30, 270, 181, 17))
        self.checkBox.setObjectName(_fromUtf8("checkBox"))

        self.retranslateUi(Dialog)
        QtCore.QObject.connect(self.pushButton_2, QtCore.SIGNAL(_fromUtf8("clicked()")), self.lineEdit.clear)
	QtCore.QObject.connect(self.pushButton_3, QtCore.SIGNAL(_fromUtf8("clicked()")), self.lineEdit.clear)
	QtCore.QObject.connect(self.pushButton_3, QtCore.SIGNAL(_fromUtf8("clicked()")), self.textBrowser.clear)
        QtCore.QObject.connect(self.pushButton_3, QtCore.SIGNAL(_fromUtf8("clicked()")), Dialog.close)
        QtCore.QObject.connect(self.pushButton_2, QtCore.SIGNAL(_fromUtf8("clicked()")), self.textBrowser.clear)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(_translate("Dialog", "HUFFMAN 1", None))
        self.pushButton.setText(_translate("Dialog", "Generuj", None))
        self.label.setText(_translate("Dialog", "Wprowadz tekst do zakodowania:", None))
        self.pushButton_2.setText(_translate("Dialog", "Czyść", None))
        self.pushButton_3.setText(_translate("Dialog", "Zamknij", None))
	self.pushButton_4.setText(_translate("Dialog", "Losowo", None))
	self.checkBox.setText(_translate("Dialog", "Użyj słownika angielskiego", None))
