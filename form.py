from PyQt4 import QtCore, QtGui
from PyQt4.QtGui import *

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName(_fromUtf8("Form"))
        Form.setFixedSize(400, 300)
	palette	= QPalette()
    	palette.setBrush(QPalette.Background,QBrush(QPixmap("tlo.jpg")))
    	Form.setPalette(palette)
        self.label_6 = QtGui.QLabel(Form)
        self.label_6.setGeometry(QtCore.QRect(130, 30, 241, 20))
        font = QtGui.QFont()
        font.setPointSize(16)
        self.label_6.setFont(font)
        self.label_6.setObjectName(_fromUtf8("label_6"))
        self.pushButton = QtGui.QPushButton(Form)
        self.pushButton.setGeometry(QtCore.QRect(170, 250, 75, 23))
        self.pushButton.setObjectName(_fromUtf8("pushButton"))
	self.pushButton.setStyleSheet("background-color: cyan")
        self.label = QtGui.QLabel(Form)
        self.label.setGeometry(QtCore.QRect(10, 60, 381, 181))
        self.label.setObjectName(_fromUtf8("label"))

        self.retranslateUi(Form)
        QtCore.QObject.connect(self.pushButton, QtCore.SIGNAL(_fromUtf8("clicked()")), Form.close)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        Form.setWindowTitle(_translate("Form", "Info", None))
        self.label_6.setText(_translate("Form", "Kodowanie Huffmana", None))
        self.pushButton.setText(_translate("Form", "OK", None))
        self.label.setText(_translate("Form", "\t\tAutorzy:\n\t\t-Rafal Palej\n\t\t-Szymon Dabrowki\nDostepne opcje kodowania:\n1)Obliczanie dynamiczne na podstawie czestosci znakow\n2)Slownik j. ang\n3)Zrodlo podane recznie\nProgram wizualizuje drzewo kodowe\n\n\t\tCopyright 2014 All Rights Reserved", None))