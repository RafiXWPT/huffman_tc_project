# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'huf2.ui'
#
# Created: Thu Jun 12 23:25:53 2014
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui
from PyQt4.QtGui import *

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Dialog2(object):
    def setupUi(self, Dialog2):
        Dialog2.setObjectName(_fromUtf8("Dialog2"))
        Dialog2.setFixedSize(400, 300)
	palette	= QPalette()
    	palette.setBrush(QPalette.Background,QBrush(QPixmap("imag.jpg")))
    	Dialog2.setPalette(palette)
        self.textBrowser = QtGui.QTextBrowser(Dialog2)
        self.textBrowser.setGeometry(QtCore.QRect(20, 70, 251, 192))
        self.textBrowser.setObjectName(_fromUtf8("textBrowser"))
        self.pushButton_3 = QtGui.QPushButton(Dialog2)
        self.pushButton_3.setGeometry(QtCore.QRect(300, 25, 75, 31))
        self.pushButton_3.setObjectName(_fromUtf8("pushButton_3"))
        self.lineEdit = QtGui.QLineEdit(Dialog2)
        self.lineEdit.setGeometry(QtCore.QRect(20, 30, 251, 20))
        self.lineEdit.setObjectName(_fromUtf8("lineEdit"))
        self.pushButton = QtGui.QPushButton(Dialog2)
        self.pushButton.setGeometry(QtCore.QRect(300, 80, 75, 31))
        self.pushButton.setObjectName(_fromUtf8("pushButton"))
        self.pushButton_2 = QtGui.QPushButton(Dialog2)
        self.pushButton_2.setGeometry(QtCore.QRect(300, 120, 75, 31))
        self.pushButton_2.setObjectName(_fromUtf8("pushButton_2"))
        self.pushButton_4 = QtGui.QPushButton(Dialog2)
        self.pushButton_4.setGeometry(QtCore.QRect(300, 230, 75, 31))
        self.pushButton_4.setObjectName(_fromUtf8("pushButton_4"))
	self.pushButton_5 = QtGui.QPushButton(Dialog2)
	self.pushButton_5.setGeometry(QtCore.QRect(300, 160, 75, 31))
        self.pushButton_5.setObjectName(_fromUtf8("pushButton_4"))
	self.pushButton.setStyleSheet("background-color: cyan")
	self.pushButton_2.setStyleSheet("background-color: cyan")
	self.pushButton_3.setStyleSheet("background-color: cyan")
	self.pushButton_4.setStyleSheet("background-color: cyan")
	self.pushButton_5.setStyleSheet("background-color: cyan")
        self.label = QtGui.QLabel(Dialog2)
        self.label.setGeometry(QtCore.QRect(20, 10, 131, 16))
        self.label.setObjectName(_fromUtf8("label"))
	self.checkBox2 = QtGui.QCheckBox(Dialog2)
        self.checkBox2.setGeometry(QtCore.QRect(30, 270, 181, 17))
        self.checkBox2.setObjectName(_fromUtf8("checkBox2"))

        self.retranslateUi(Dialog2)
        QtCore.QMetaObject.connectSlotsByName(Dialog2)
	QtCore.QObject.connect(self.pushButton_2, QtCore.SIGNAL(_fromUtf8("clicked()")), self.lineEdit.clear)
	QtCore.QObject.connect(self.pushButton_2, QtCore.SIGNAL(_fromUtf8("clicked()")), self.textBrowser.clear)
	QtCore.QObject.connect(self.pushButton_4, QtCore.SIGNAL(_fromUtf8("clicked()")), self.textBrowser.clear)
	QtCore.QObject.connect(self.pushButton_4, QtCore.SIGNAL(_fromUtf8("clicked()")), self.lineEdit.clear)
	QtCore.QObject.connect(self.pushButton_4, QtCore.SIGNAL(_fromUtf8("clicked()")), Dialog2.close)

    def retranslateUi(self, Dialog2):
        Dialog2.setWindowTitle(_translate("Dialog2", "HUFFMAN 2", None))
        self.pushButton_3.setText(_translate("Dialog2", "Otwórz", None))
        self.pushButton.setText(_translate("Dialog2", "Generuj", None))
        self.pushButton_2.setText(_translate("Dialog2", "Czyść", None))
        self.pushButton_4.setText(_translate("Dialog2", "Zamknij", None))
	self.pushButton_5.setText(_translate("Dialog2", "Ustawienia", None))
        self.label.setText(_translate("Dialog2", "Wybierz plik:", None))
	self.checkBox2.setText(_translate("Dialog", "Użyj wlasnych parametrow", None))
