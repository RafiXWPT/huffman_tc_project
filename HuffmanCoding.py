from __future__ import division
import os, sys
from PyQt4 import QtCore, QtGui
from PyQt4.QtGui import *
from PyQt4.QtCore import *
from okna2 import Ui_Dialog
from form import Ui_Form
from wyb import Ui_Wyb
from huf2 import Ui_Dialog2
from mywindow import Ui_MyWindow
from collections import Counter
import networkx as nx
import matplotlib.pyplot as plt
import math
import heapq
import string
import random
import re

TXT = ''
TXT2BIT = []
BIT2TXT = []
HUFF_OUTPUT = []
ANG_ALPH = [('e',9.57),('t',6.68),('a',6.22),('o',5.48),('i',5.11),('s',5.03),('n',4.92),('h',4.72),('r',4.58),('d',3.51),('l',3.36),('u',2.25),('m',1.97),('c',1.91),('f',1.65),('g',1.62),('w',1.59),('y',1.52),('p',1.46),('b',1.18),('k',0.849),('v',0.605),('j',0.112),('x',0.091),('q',0.077),('z',0.057),('0',1.00),('1',1.00),('2',1.00),('3',1.00),('4',1.00),('5',1.00),('6',1.00),('7',1.00),('8',1.00),('9',1.00),(' ',1.00)]
lit_tmp=['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','w','v','x','y','z','0','1','2','3','4','5','6','7','8','9']
prawd_tmp=['1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1']
ALPH = []

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Node(object):
    def __init__(self, pairs, frequency):                               
        self.pairs = pairs                                              
        self.frequency = frequency                                      

    def __repr__(self):
        return repr(self.pairs) + ", " + repr(self.frequency)

    
    def merge(self, other):
        total_frequency = self.frequency + other.frequency
        for p in self.pairs:
            p[1] = "0" + p[1]
        for p in other.pairs:
            p[1] = "1" + p[1]
        new_pairs = self.pairs + other.pairs
        return Node(new_pairs, total_frequency)

    def __lt__(self, other):
        return self.frequency < other.frequency

    
def huffman_code(s, flag, define):
    table = []
    TAB = []
    global ALPH
    if flag == 0:
        table = [Node([[ch, '']], freq) for ch, freq in Counter(s).items()]
    else:
    	if define == 0:
    		TAB = ANG_ALPH
    	else:
    		TAB = Tab().TabInsert()
        s = ''.join(set(s))
        for i in xrange(len(s)):
            for key, value in TAB:
                if s[i].isupper():
                    if string.lower(s[i]) ==  key:
                        table.append(Node([[string.upper(key), '']], value))
                else:
                    if s[i] == key:
                        table.append(Node([[key, '']], value))
    heapq.heapify(table)
                                                
    while len(table) > 1:                  
        first_node = heapq.heappop(table)                               
        second_node = heapq.heappop(table)                              
        new_node = first_node.merge(second_node)                        
        heapq.heappush(table, new_node)     
    return dict(table[0].pairs) 

def code(text):
    global TXT, TXT2BIT, HUFF_OUTPUT
    if len(HUFF_OUTPUT) == 1:
        for i in xrange(len(TXT)):
            TXT2BIT.append("0")
        TXT2BIT = ''.join(TXT2BIT)
    else:
        for i in xrange(len(TXT)):
            u = TXT[i]
            for x, y in HUFF_OUTPUT:
                if u == x:
                    TXT2BIT.append(y)
        TXT2BIT = ''.join(TXT2BIT)
    
def decode(bitstream):
    global TXT2BIT, BIT2TXT, HUFF_OUTPUT
    if len(HUFF_OUTPUT) == 1:
        for i in xrange(len(TXT2BIT)):
            BIT2TXT.append(TXT[0])
        BIT2TXT = ''.join(BIT2TXT)
    else:
        tmp = ''
        for i in xrange(len(TXT2BIT)):
            tmp = tmp + TXT2BIT[i]
            for x, y in HUFF_OUTPUT:
                if tmp == y:
                    BIT2TXT.append(x)
                    tmp = ''
        BIT2TXT = ''.join(BIT2TXT)                                      


def code_prop(prawd, kod):
    x, y, z = 0, 0, 0
    for i in xrange(len(prawd)):
        if len(prawd) == 1:
            x = 1
            y = -1
        else:
            x = x + prawd[i]*len(kod[i])
            y = y + prawd[i]*math.log(prawd[i],2)
    z = -1*(y/x)*100
    return x, -y, z

def f5(seq, idfun=None): 
   if idfun is None:
       def idfun(x): return x
   seen = {}
   result = []
   for item in seq:
       marker = idfun(item)
       if marker in seen: continue
       seen[marker] = 1
       result.append(item)
   return result 
    
def draw_graph(graph):
    nodes = set([n1 for n1, n2, n3 in graph] + [n2 for n1, n2, n3 in graph])
    G=nx.DiGraph()
    for node in nodes:
        G.add_node(node)
    for edge in graph:
        G.add_edge(edge[0], edge[1], Node=edge[2])
    edge_labels = dict([((u,v,),d['Node']) for u,v,d in G.edges(data=True)])
    nx.write_dot(G,'test.dot')
    plt.title("Pelne Drzewko Kodowe")
    pos=nx.graphviz_layout(G,prog='dot')
    nx.draw(G,pos,node_size=2000,with_labels=True,with_edges=True,edge_labels=edge_labels)
    nx.draw_networkx_edge_labels(G,pos,edge_labels=edge_labels)
    plt.savefig("graph.png")
    plt.show()
    
def prepare_graph():
	graph = []
	save = []
	toadd = []
	max = 0
	for k,v in HUFF_OUTPUT:
  		if len(v) > max:
    			max = len(v)

	for k,v in HUFF_OUTPUT:
    		if v == '0':
      			graph.append(('R',0,k))
		if len(graph) == 0:
  			graph.append(('R','0',''))
  
	for k,v in HUFF_OUTPUT:
    		if v == '1':
      			graph.append(('R','1',k))
		if len(graph) == 1:
  			graph.append(('R','1',''))
  
	for y in xrange(1,max+1):
  		for x in xrange(pow(2,max)):
    			temp = "{0:b}".format(x).zfill(y)
    			save.append(temp)
  			for k,v in HUFF_OUTPUT:
    				if (str(temp)==str(v) and str(temp)!='0' and str(temp)!='1'):
      					graph.append((temp[:len(temp)-1], temp, k))
      	graph = f5(graph)
      	
      	for i in save:
    		toadd.append((i[:(len(i)-1)],i,''))
	toadd.pop(0)
	toadd.pop(0)
	
	for x,y,z in graph:
		for a,b,c in toadd:
			if (x == a and y == b):
				toadd.remove((a,b,c))
	graph = graph + toadd		
	return f5(graph)

def Main(s, f, d):
    global ALPH, TXT, TXT2BIT, BIT2TXT, HUFF_OUTPUT, SRDL, ENTR, EFF, k, v
    TXT2BIT, BIT2TXT, t_p, t_c, t_l, SRDL, ENTR, EFF = [], [], [], [], [], 0, 0, 0
    TXT = s
    tablica_asocjacji = huffman_code(s, f, d)
    wyst = Counter(s)
    total = len(s)

    " " " Tworzenie Tablic z Danymi " " "
    for i, v in wyst.items():
        t_p.append(v/total)
    for wartosc in tablica_asocjacji.itervalues():
        t_c.append(wartosc)
    for klucz in tablica_asocjacji:
        t_l.append(klucz)

    " " " Utworzenie Slownika Symboli i Odpowiadajacych Im Kodow " " "
    HUFF_OUTPUT = sorted(dict(zip(t_l,t_c)).iteritems(), key=lambda t: (len(t[1]), t[1]))

    " " " Kodowanie " " "
    code(TXT)

    " " " Dekodowanie " " "
    decode(TXT2BIT)

    " " " Srednia Dlugosc Kodu / Entropia Zrodla / Efektywnosc Kodu " " "
    SRDL, ENTR, EFF = code_prop(t_p, t_c)

class MyDialog(QtGui.QDialog):
    def __init__(self, parent=None):
        super(MyDialog, self).__init__(parent)
   	self.ui = Ui_Dialog()
	self.ui.setupUi(self)	
        QtCore.QObject.connect(self.ui.pushButton,QtCore.SIGNAL("clicked()"), self.ui.textBrowser.clear)
	QtCore.QObject.connect(self.ui.pushButton,QtCore.SIGNAL("clicked()"), self.nowa)
	QtCore.QObject.connect(self.ui.pushButton_4,QtCore.SIGNAL("clicked()"), self.rand_word)
	QtCore.QObject.connect(self.ui.pushButton_3,QtCore.SIGNAL("clicked()"), self.unchecked)

    def unchecked(self):
	self.ui.checkBox.setCheckState(QtCore.Qt.Unchecked)

    def nowa(self):
	if self.ui.lineEdit.text() != "": 
		tekst=str(self.ui.lineEdit.text())
		s=re.sub(r'([^\s\w]|_)+', '', tekst)
		if self.ui.checkBox.isChecked():
			flag=1
		else:	
			flag=0
		Main(s, flag, 0)
		self.ui.textBrowser.append("SYMBOL\tCODE")
           	if len(HUFF_OUTPUT) == 1:
                	self.ui.textBrowser.append("%s\t%s" % (TXT[0], 0))
            	else:
                	for k, v in HUFF_OUTPUT: 
                  		self.ui.textBrowser.append("%s\t%s" % (k, v))
		self.ui.textBrowser.append("\nSrednia dlugosc kodu: %g\nEntropia zrodla: %g\nEfektywnosc kodu: %g%s\n\nPodany tekst: %s\nZakodowany tekst: %s\nOdkodowany tekst: %s" % (round(SRDL,3), round(ENTR,3), round(EFF,3), "%", TXT, TXT2BIT, BIT2TXT))
		tomake = prepare_graph()
    		draw_graph(tomake) 	
	else:
        	self.ui.textBrowser.append("Podaj tekst do zakodowania !!!")
	
            	     	
    
    def rand_word(self):
	self.ui.lineEdit.setText(''.join(random.choice(string.ascii_uppercase + string.digits + string.ascii_lowercase) for _ in range(30)))

class Tab(QtGui.QDialog):
    def __init__(self, parent=None):
        super(Tab, self).__init__(parent)
   	self.ui = Ui_Wyb()
	self.ui.setupUi(self)
	QtCore.QObject.connect(self.ui.pushButton_3, QtCore.SIGNAL(_fromUtf8("clicked()")), self.Tabreset)
	QtCore.QObject.connect(self.ui.pushButton_4, QtCore.SIGNAL(_fromUtf8("clicked()")), self.TabInsert)

    def Tabreset(self):
	ALPH = []
	for i in xrange(35):
		item = self.ui.tableWidget.item(i, 1)
        	item.setText(_translate("Wyb", "1", None))
    
    def TabInsert(self):
	global ALPH
	global lit_tmp
	global prawd_tmp
	pole_lit = self.ui.lineEdit.text()
	pole_prawd = self.ui.lineEdit_2.text()
	for i in range(0, 36):
		if lit_tmp[i] == pole_lit:
			prawd_tmp[i] = str(pole_prawd)
			item = self.ui.tableWidget.item(i, 1)
        		item.setText(_translate("Wyb", str(pole_prawd), None))
	ALPH = []
	for x in range(0, 36):
		ALPH.append((lit_tmp[x],prawd_tmp[x]))
	ALPH.append((' ', '1'))
	self.ui.lineEdit.setText("")
	self.ui.lineEdit_2.setText("")
	
	return ALPH
		
class MyDialog2(QtGui.QDialog):
    def __init__(self, parent=None):
        super(MyDialog2, self).__init__(parent)
   	self.ui = Ui_Dialog2()
	self.ui.setupUi(self)   
	QtCore.QObject.connect(self.ui.pushButton,QtCore.SIGNAL("clicked()"), self.ui.textBrowser.clear)
	QtCore.QObject.connect(self.ui.pushButton,QtCore.SIGNAL("clicked()"), self.nowa)
	QtCore.QObject.connect(self.ui.pushButton_3,QtCore.SIGNAL("clicked()"), self.selectFile) 
	QtCore.QObject.connect(self.ui.pushButton_5,QtCore.SIGNAL("clicked()"), self.on_pushButton_clicked5)
	QtCore.QObject.connect(self.ui.pushButton_4,QtCore.SIGNAL("clicked()"), self.unchecked)
    
	self.dialogTextBrowser5 = Tab(self)
	
    def unchecked(self):
	self.ui.checkBox2.setCheckState(QtCore.Qt.Unchecked)

    @QtCore.pyqtSlot()
    def on_pushButton_clicked5(self):
	self.dialogTextBrowser5.exec_()
 
    

    def selectFile(self):
	filename = QtGui.QFileDialog.getOpenFileName(self, 'Open File', '.')
	if filename != "":
		fname = open(filename)
		data = fname.read()
		self.ui.lineEdit.setText(data)
		fname.close() 


    def nowa(self):
		for x in range(0,36):
			ALPH.append((lit_tmp[x],prawd_tmp[x]))
		ALPH.append((' ', '1'))
		if self.ui.lineEdit.text() != "": 
			tekst=str(self.ui.lineEdit.text())
			s=re.sub(r'([^\s\w]|_)+', '', tekst)	
			if self.ui.checkBox2.isChecked():
				d=1
				flag=1
			else:	
				d=0	
				flag=0
			Main(s, flag, d)
			self.ui.textBrowser.append("SYMBOL\tCODE")
            		if len(HUFF_OUTPUT) == 1:
                		self.ui.textBrowser.append("%s\t%s" % (TXT[0], 0))
            		else:
                		for k, v in HUFF_OUTPUT: 
                    			self.ui.textBrowser.append("%s\t%s" % (k, v))
			self.ui.textBrowser.append("\nSrednia dlugosc kodu: %g\nEntropia zrodla: %g\nEfektywnosc kodu: %g%s\n\nPodany tekst: %s\nZakodowany tekst: %s\nOdkodowany tekst: %s" % (round(SRDL,3), round(ENTR,3), round(EFF,3), "%", TXT, TXT2BIT, BIT2TXT))
			tomake = prepare_graph()
    			draw_graph(tomake)		
		else:
            		self.ui.textBrowser.append("Wczytaj plik !!!")

class Info(QtGui.QDialog):
    def __init__(self, parent=None):
        super(Info, self).__init__(parent)
   	self.ui = Ui_Form()
	self.ui.setupUi(self)

class MyWindow(QtGui.QMainWindow):
    def __init__(self, parent=None):
        super(MyWindow, self).__init__(parent)
   	self.ui = Ui_MyWindow()
	self.ui.setupUi(self)  
	self.statusBar().showMessage('Program gotowy')
	QtCore.QObject.connect(self.ui.pushButton,QtCore.SIGNAL("clicked()"), 	self.on_pushButton_clicked)
	QtCore.QObject.connect(self.ui.pushButton_2,QtCore.SIGNAL("clicked()"), self.on_pushButton_clicked2)
	QtCore.QObject.connect(self.ui.actionKnmk,QtCore.SIGNAL('triggered()'), self.on_pushButton_clicked)
	QtCore.QObject.connect(self.ui.actionMl,QtCore.SIGNAL('triggered()'), self.on_pushButton_clicked2)
	QtCore.QObject.connect(self.ui.actionInfo,QtCore.SIGNAL('triggered()'), self.on_pushButton_clicked3)

	self.dialogTextBrowser = MyDialog(self)
	self.dialogTextBrowser2 = MyDialog2(self)
	self.dialogTextBrowser3 = Info(self)

    @QtCore.pyqtSlot()
    def on_pushButton_clicked(self):
        self.dialogTextBrowser.exec_()
    
    @QtCore.pyqtSlot()
    def on_pushButton_clicked2(self):
        self.dialogTextBrowser2.exec_()	

    @QtCore.pyqtSlot()
    def on_pushButton_clicked3(self):
        self.dialogTextBrowser3.exec_()	

if __name__ == "__main__":
    import sys

    app = QtGui.QApplication(sys.argv)
    app.setApplicationName('MyWindow')

    main = MyWindow()
    main.show()

    sys.exit(app.exec_())